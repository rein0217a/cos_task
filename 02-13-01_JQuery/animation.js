$(document).ready(function(){
    // table初期化
    initTable();

    // 検索ボタン押下
    $("#btn_search").on("click", function(){
        $("tr").each(function(index){
            if(index == 0){
                return true;
            }
            var txt_search = $("#input_search").val();
            var txt_target = $(this).text();
            if(txt_target.match(txt_search) != null){
                $(this).show();
            }else{
                $(this).hide();
            }
        })
    })
});



// table初期化関数
function initTable(){
    let constellationLst = [
        ["牡羊座", "Aries"],
        ["牡牛座", "Taurus"],
        ["双子座", "Gemini"],
        ["蟹座", "Cancer"],
        ["獅子座", "Leo"],
        ["乙女座", "Virgo"],
        ["天秤座", "Libra"],
        ["蠍座", "Scorpio"],
        ["射手座", "Sagittarius"],
        ["山羊座", "Capricorn"],
        ["水瓶座", "Aquarius"],
        ["魚座", "Pisces"]
    ]
    
    for(var i = 0; i < constellationLst.length; i++){
        var str = "<tr>";
        str += "<td>";
        str += i;
        str += "</td>";
        for(var j = 0; j < 2; j++){
            str += "<td>"
            str += constellationLst[i][j];
            str += "</td>";
        }
        str += "</tr>";
        $("table").append(str);
    }
}