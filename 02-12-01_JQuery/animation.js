$(document).ready(function(){
     let cls_navigation = ".navigation";
     let offsetTop_back =$(".back").offset().top;

     // back初期化
     for(var i = 1; i <= 200; i++){
          $(".back").append("<p>" + i + "</p>");
     }
     // navigationのtopを初期化
     $(cls_navigation).css("top", offsetTop_back);

     // scrollしたら、navのbottom値を変更
     $(document).on("scroll", function(){
          var distance= $(this).scrollTop();
          if(distance == 0){
               $(cls_navigation).css("top", offsetTop_back);
          }else if(distance >= offsetTop_back){
               $(cls_navigation).css("top", 0);
          }else{
               $(cls_navigation).css("top", offsetTop_back - distance);
          }
     });

     // btn_nav押下
     var flg = true;
     $(".btn_nav").on("click", function(ev){
          ev.preventDefault();
          if(flg){
               $(cls_navigation).css("transform", "translateX(0)");
               flg = false;
          }else{
               $(cls_navigation).css("transform", "translateX(-232px)");
               flg = true;
          }
     });  
});