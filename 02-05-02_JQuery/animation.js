$(document).ready(function(){
    $(".btn_search").on("click", function(event){
        var input_txt = $(".input_txt").val();
        var url = "./json/"+ input_txt +".json";

        $.ajax({
            url: url,
            type: "GET",
            dotaType: "json",
            timeout: 10000,
            error:function(){
                $("#output_txearea").text("読み込み失敗");
            },
            success:function(data){
                $("#output_txearea").load(url);
            }
        });
    });
});