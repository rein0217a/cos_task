$(document).ready(function(){
    // 初期設定
    let date = new Date();
    $("#origin_input").val(changeFormat(date, "YYYY年MM月DD日"));

    $("#btn_change").on("click", function(){
        $("#change_input").val(changeFormat(date, "YYYY/MM/DD"));
    });
});

function changeFormat(date, format){
    var format = format.replace("YYYY", date.getFullYear());
    var format = format.replace("MM", date.getMonth() + 1);
    var format = format.replace("DD", date.getDate());

    return format;
}