
window.onload = main;
window.onscroll = scrollMain;
document.onclick = onclickMain;

// ページ読み込み完了後、実行関数
function main(){
    // topへボタンを非表示
    topBtnRemove();
}

// スクロール後、実行関数
function scrollMain(){
    // topへボタンの表示／非表示切替
    topBtnAnimation();
    // logoのテキスト非表示
    logoAnimation(400);
}

// クリック後、実行関数
function onclickMain(ele){
    for(var i=0; i < ele.path.length; i++){
        // element要素がaの場合
        if(ele.path[i].localName == "a"){
            var targetHref = ele.path[i].hash;

            // メニューボタンがクリックされたら、
            // メニュー表示アニメーションを実行
            if(targetHref == "#headerNav"){
                ele.preventDefault();
                menuAnimation(ele.path[i]);
                break;
            
            // タブがクリックされたら、対応したパネルを表示／非表示する
            }else{
                break;
            }
        }
    }   
}