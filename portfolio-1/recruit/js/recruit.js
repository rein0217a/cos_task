
window.onload = main;
window.onscroll = scrollMain;
document.onclick = onclickMain;

// ページ読み込み完了後、実行関数
function main(){
    // topへボタンを非表示
    topBtnRemove();
    // logoのテキスト非表示
    logoAnimation(0);

    // tabMenu_HumanCoordとpanelHumanCoordを非表示
    document.getElementById("tabMenu_HumanCoord").classList.add("aboutJobs_tabOff");
    document.getElementById("panelHumanCoord").style.display = "none";
}

// スクロール後、実行関数
function scrollMain(){
    // topへボタンの表示／非表示切替
    topBtnAnimation();
}

// クリック後、実行関数
function onclickMain(ele){
    for(var i=0; i < ele.path.length; i++){
        // element要素がaの場合
        if(ele.path[i].localName == "a"){
            var targetHref = ele.path[i].hash;

            // メニューボタンがクリックされたら、
            // メニュー表示アニメーションを実行
            if(targetHref == "#headerNav"){
                ele.preventDefault();
                menuAnimation(ele.path[i]);
                break;
            
            // タブがクリックされたら、対応したパネルを表示／非表示する
            }else if((targetHref == "#panelEngineer") || 
                        (targetHref == "#panelHumanCoord")){
                ele.preventDefault();

                // panelEngineerの場合
                if(targetHref == "#panelEngineer"){
                    // panelEngineerを表示
                    document.getElementById("tabMenu_Engineer").classList.remove("aboutJobs_tabOff");
                    document.getElementById("panelEngineer").style.display = "block";
                    // panelHumanCoordを非表示
                    document.getElementById("tabMenu_HumanCoord").classList.add("aboutJobs_tabOff");
                    document.getElementById("panelHumanCoord").style.display = "none";

                // panelHumanCoordの場合
                }else if(targetHref == "#panelHumanCoord"){
                    // panelHumanCoordを表示
                    document.getElementById("tabMenu_HumanCoord").classList.remove("aboutJobs_tabOff");
                    document.getElementById("panelHumanCoord").style.display = "block";
                    // panelEngineerを非表示
                    document.getElementById("tabMenu_Engineer").classList.add("aboutJobs_tabOff");
                    document.getElementById("panelEngineer").style.display = "none";
                }
                break;

            }else{
                break;
            }
        }
    }   
}