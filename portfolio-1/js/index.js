
window.onload = main;
window.onscroll = scrollMain;
document.onclick = onclickMain;

// ページ読み込み完了後、実行関数
function main(){
    // topへボタンを非表示
    topBtnRemove();

    // コンテンツ非表示
    function conReActive(con){
        // isActiveクラス削除
        document.getElementsByClassName(con)[0].classList.remove("isActive");
    }
    conReActive("message");
    conReActive("company");
    conReActive("company_infomation");
    conReActive("company_member");
    conReActive("company_history");
    conReActive("structure");
    conReActive("service");
    conReActive("recruit");
    conReActive("access");
    
    // pc版だった場合、img置換
    const breakPoint = 640;
    var winWidth = window.innerWidth;
    if(winWidth >= breakPoint){
        var list = document.getElementsByClassName("diffImg");
        for(var i=0; i < list.length; i++){
            var targetSrc = list[i].getAttribute("src").replace("/sp", "");
            list[i].setAttribute("src", targetSrc);
        }
    }
}

// スクロール後、実行関数
function scrollMain(){
    // logoのテキスト表示／非表示
    logoAnimation(400);
    // topへボタンの表示／非表示切替
    topBtnAnimation();

    var nowPoint = window.pageYOffset;
    // コンテンツふわっと出現
    function addIsShow(con){
        // 対象コンテンツのクラスにisShowがない場合
        // isShowクラスを追加
        var target = document.getElementsByClassName(con)[0].classList;
        var result = target.contains("isShow");
        if (!result){
            target.add("isShow");
        }
    }
    if(nowPoint >= 400){
        addIsShow("message");
    }
    if(nowPoint >= 1715){
        addIsShow("company");
        addIsShow("company_infomation");
    }
    if(nowPoint >= 3020){
        addIsShow("company_member");
        addIsShow("company_history");
    }
    if(nowPoint >= 4175){
        addIsShow("structure");
    }
    if(nowPoint >= 4960){
        addIsShow("service");
    }
    if(nowPoint >= 6950){
        addIsShow("recruit");
    }
    if(nowPoint >= 7850){
        addIsShow("access");
    }
}

// クリック後、実行関数
function onclickMain(ele){
    for(var i=0; i < ele.path.length; i++){
        // element要素がaの場合
        if(ele.path[i].localName == "a"){
            var targetHref = ele.path[i].hash;

            // メニューボタンがクリックされたら、
            // メニュー表示アニメーションを実行
            if(targetHref == "#headerNav"){
                ele.preventDefault();
                menuAnimation(ele.path[i]);
                break;

            // それ以外はスムーズスクロールを実行
            }else if(targetHref.includes("#")){
                ele.preventDefault();
                document.querySelector(targetHref).scrollIntoView({
                    behavior: "smooth",
                    block: "start"
                });
                // メニューを閉じる
                if(document.getElementById("headerNav").classList.contains("active")){
                    menuAnimation(document.getElementsByClassName("header_btnNav_inner")[0]);
                }
                break;

            }else{
                break;
            }
        }
    }   
}