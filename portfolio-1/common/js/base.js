
// topへボタンを非表示
function topBtnRemove(){
    document.getElementById("btn_toTop").classList.remove("isActive");
}

// topへボタンの表示／非表示切替
function topBtnAnimation(){
    var nowPoint = window.pageYOffset;
    var btnToTop = document.getElementById("btn_toTop");
    if(nowPoint >= 400){
        // 表示
        btnToTop.classList.add("isShow");
    }else if(nowPoint < 400){
        // 非表示
        btnToTop.classList.remove("isShow");
    }
}

// logoのテキスト表示／非表示
function logoAnimation(breakPoint){
    var nowPoint = window.pageYOffset;
    var logo = document.getElementsByClassName("header_logo")[0];

    if(nowPoint >= breakPoint){
        // 非表示
        logo.classList.remove("default");
        requestAnimationFrame(function () {
            logo.classList.add("active");
        });
    }else if(nowPoint < breakPoint){
        // 表示
        logo.classList.remove("active");
        requestAnimationFrame(function () {
            logo.classList.add("default");
        });
    }
}

// メニュー表示／非表示
function menuAnimation(menuBtn){
    var menu = document.getElementById("headerNav");
    var menuInner = document.getElementsByClassName("headerNav_inner")[0];
    var menuBg = document.getElementsByClassName("headerBg")[0];

    if (menu.classList.contains("active") &&
        menuInner.classList.contains("active") &&
        menuBtn.classList.contains("active") &&
        menuBg.classList.contains("active")) {

        // 非表示
        menu.classList.remove("active");
        menuInner.classList.remove("active")
        menuBtn.classList.remove("active");
        menuBg.classList.remove("active")

        requestAnimationFrame(function () {
            menu.classList.add("default");
            menuInner.classList.add("default");
            menuBtn.classList.add("default");
            menuBg.classList.add("default")
        });

    } else {
        // 表示
        menu.classList.remove("default");
        menuInner.classList.remove("default");
        menuBtn.classList.remove("default");
        menuBg.classList.remove("default")

        requestAnimationFrame(function () {
            menu.classList.add("active");
            menuInner.classList.add("active");
            menuBtn.classList.add("active");
            menuBg.classList.add("active")
        });
        
    }
}