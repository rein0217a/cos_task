// 初期表示日付
let nowDate = new Date();
let now_year = nowDate.getFullYear();
let now_month = nowDate.getMonth() + 1;
let now_date = nowDate.getDate();
// カレンダー表示日付
var year = now_year;
var month = now_month;
var date = now_date;
// クリックされた日付
var click_year;
var click_month;
var click_date;

$(document).ready(function(){
    $(".popup").hide();
    $(".back").hide();

    // カレンダー作成
    initCalender(year, month, date);

    // 前へボタン押下
    $("#btn_prev").on("click", function(){
        if(month == 1){
            year -= 1;
            month = 12;
        }else{
            month -= 1;
        }
        if((year == now_year) && (month == now_month)){
            date = now_date;
        }else{
            date = 0;
        }
        initCalender(year, month, date);
    });

    // 次へボタン押下
    $("#btn_next").on("click", function(){
        if(month == 12){
            year += 1;
            month = 1;
        }else{
            month += 1;
        }
        if((year == now_year) && (month == now_month)){
            date = now_date;
        }else{
            date = 0;
        }
        initCalender(year, month, date);
    });

    // カレンダー開くボタン押下
    $(".btn_open").on("click", function(){
        $(".popup").show();
        $(".back").show();
    });

    // カレンダー閉じるボタン押下
    $(".btn_close").on("click", function(){
        $(".popup").hide();
        $(".back").hide();
    });
});



// カレンダー作成関数
function initCalender(year, month, date){
    $('#yearMonth').text(year + '年' + month + '月');

    var firstDate = new Date(year, month, 1);
    var lastDate = new Date(year, month, 0);

    // 一度カレンダー削除
    $('tr', 'tbody').remove();

    // 一行目のカレンダー作成
    var dateCount = firstDate.getDate();
    var str = '<tr>'
    for(var i=0; i<7; i++){        
        if(i >= firstDate.getDay()){
            str += '<td>'
            str += dateCount;
            dateCount++;
        }else{
            str += '<td>'
            str += '&nbsp';
        }
        str += '</td>'
    }
    str += '</tr>'
    $('tbody').append(str);

    // 一行目以降のカレンダー作成
    var num_row = (lastDate.getDate() - dateCount) / 7;
    for(var i = 0; i <= num_row; i++){
        var str = '<tr>';
        for(var j=0; j<7; j++){
            str += '<td>'
            if(dateCount > lastDate.getDate()){
                str += '&nbsp';
            }else{
                str += dateCount;
                dateCount++;
            }
        }
        str += '</tr>'
        $('tbody').append(str);
    }

    // 今日の日付にクラスを付与
    addTodayClass();

    // 祝日設定
    setHoliday();

    // クリックされた日付があれば、
    // 日付クリッククラスを付与
    addClickClass();

    // 日付クリック
    $("td", "tbody").on("click", function(){
        var txt_date = $(this).text();
        $("#input_date").val(year + "/" + month + "/" + txt_date);
        click_year = year;
        click_month = month;
        click_date = txt_date;
        removeClickClass();
        addClickClass();
    });
}

// 今日の日付にクラスを付与関数
function addTodayClass(){
    if((year == now_year) && (month == now_month)){
        $("td", "tbody").each(function(){
            if($(this).text() == now_date){
                $(this).addClass("is-today");
            }
        })
    }
}

// クリックされた日付にクラスを付与関数
function addClickClass(){
    if((year == click_year) && (month == click_month)){
        $("td", "tbody").each(function(){
            if($(this).text() == click_date){
                $(this).addClass("is-click");
            }
        })
    }
}
// 一つ前のクリックされた日付のクラスを削除関数
function removeClickClass(){
    $("td", "tbody").each(function(){
        if($(this).attr("class")){
            $(this).removeClass("is-click");
        }
    })
}

// 祝日設定関数
function setHoliday(){
    var req = new XMLHttpRequest();
    req.open("GET", "./csv/syukujitsu.csv", true);
    req.send(null);
    req.onload = function() {
        var tmp = req.responseText.split("\n");
        for(var i=0; i< tmp.length; i++){
            var tmp_target = tmp[i].split(",")[0];
            $("td", "tbody").each(function(){
                var cal_target = year + "/" + month + "/" + $(this).text();
                if(cal_target == tmp_target){ 
                    $(this).addClass("is-holiday");
                }
            })
        }
    }
}