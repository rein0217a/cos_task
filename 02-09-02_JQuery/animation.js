$(document).ready(function(){
   // 初期設定
   $("#org_input").val("ケータイ電話はJ-PHONE");

   // 置換後１ボタン押下後
   $("#btn_change01").on("click", function(){
        var txt = $("#org_input").val().replace("J-PHONE", "Vodafone");
        $("#change01_input").val(txt);
   });

   // 置換後２ボタン押下後
   $("#btn_change02").on("click", function(){
        var txt = $("#change01_input").val().replace("Vodafone", "SoftBank");
        $("#change02_input").val(txt);
    });
});