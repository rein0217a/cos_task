let cls_ERAdressNo = ".addressNo_error";
let cls_ERMail = ".mail_error";

$(document).ready(function(){
    $("#btn_check").on("click", function(){
        // 入力フォームデータ取得
        var addressNo = $("#addressNo_input").val(),
            mail = $("#mail_input").val();

        // 郵便番号チェック
        var format_addressNo = /^\d{3}-?\d{4}$/;
        if(addressNo == ""){
            // エラーメッセージ
            setErrorMSG(cls_ERAdressNo);

        } else {
            if(addressNo.match(format_addressNo)){
                setClearMSG(cls_ERAdressNo);
            }else{
                // エラーメッセージ
                setErrorMSG(cls_ERAdressNo);
            }
        }
        // メールアドレスチェック
        var format_mail = /^([a-zA-Z0-9])+([a-zA-Z0-9\._-])*@([a-zA-Z0-9_-])+([a-zA-Z0-9\._-]+)+$/;
        if(mail == ""){
            // エラーメッセージ
            setErrorMSG(cls_ERMail);
            
        } else {
            if(mail.match(format_mail)){
                setClearMSG(cls_ERMail);
            } else {
                // エラーメッセージ
                setErrorMSG(cls_ERMail);
            }
        }
    });
});

function setErrorMSG(className){
    var txt;
    switch(className){
        case cls_ERAdressNo:
            txt = "郵便番号";
            break;
        case cls_ERMail:
            txt = "メールアドレス";
            break;
    }
    $(className).text(txt + "の形式が正しくありません。");
}
function setClearMSG(className){
    $(className).text("");
}