window.onload = tabMain;

var actTab;
var actTabBox;
var tabBox;

// メイン関数
function tabMain(){
    // 1つ目以外の、全てのタブ内容を非表示にする
    tabBox = document.getElementsByClassName("tabbox");
    for(var i=0; i < tabBox.length; i++){
        if(i == 0){
            actTabBox = tabBox[i];
            continue;
        }
        tabBox[i].style.display = "none";
    }

    // 1つ目のタブをアクティブにする
    actTab = document.getElementById("tab_list").firstElementChild.firstChild; 
    actTab.classList.add("active");
}

// タブ切り替え
function changeTab(ele){
    var clickTab = ele;
    var clickTabBox;
    for(i = 0; i < tabBox.length; i++){
        if(clickTab.getAttribute("href") == "#" + tabBox[i].id) {
            clickTabBox = tabBox[i];
        }
    }

    // タブのアクティブ状態を変更
    actTab.classList.remove("active");
    clickTab.className = "active";
    actTab = clickTab;

    // タブ内容の非表示・表示状態を変更
    actTabBox.style.display = "none";
    clickTabBox.style.display = "block";
    actTabBox = clickTabBox;

    console.log(clickTab);
}