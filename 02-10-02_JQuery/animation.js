let nowDate = new Date();
let year = nowDate.getFullYear();
let month = nowDate.getMonth() + 1;
let date = nowDate.getDate();

$(document).ready(function(){
    // select初期化
    init_select();

    // sel_monthの値を変更後
    $("#sel_year").change(function(){
        setSelectDate(Number($("#sel_year").val()) ,Number($("#sel_month").val()) ,Number($("#sel_date").val()));
    });
    // sel_yearの値を変更後
    $("#sel_month").change(function(){
        setSelectDate(Number($("#sel_year").val()) ,Number($("#sel_month").val()) ,Number($("#sel_date").val()));
    });

    // btn_search押下後
    $("#btn_search").on("click", function(){
        var result = searchZodiac(Number($("#sel_year").val()))　;
        $(".result").text(result);
    });
});



// select初期化関数
function init_select(){
    // sel_year初期化
    for(var i = year; i >= 1920; i--){
        $("#sel_year").append('<option value="' + i + '">' + i + '</option>');
    }
    $("option[value="+ (year-20) + "]", "#sel_year").prop('selected', true);

    // sel_month初期化
    for(var i = 1; i <= 12; i++){
        $("#sel_month").append('<option value="' + i + '">' + i + '</option>');
    }
    $("option[value="+ month + "]", "#sel_month").prop('selected', true);

    // sel_date初期化
    setSelectDate(year ,month ,date);
}



// select_Date設定関数
function setSelectDate(init_year ,init_month, init_date){
    if($("#sel_date").val()){
        $("#sel_date > option").remove();
    }
    var maxDate;
    switch(init_month){
        case 1:
        case 3:
        case 5:
        case 7:
        case 8:
        case 10:
        case 12:
            maxDate = 31;
            break;
        case 4:
        case 6:
        case 9:
        case 11:
            maxDate = 30;
            break;
        case 2:
            if(init_year%4 == 0){
                maxDate = 29;
            }else{
                maxDate = 28;
            }
            break;
    }
    for(var i = 1; i <= maxDate; i++){
        $("#sel_date").append('<option value="' + i + '">' + i + '</option>');
    }
    $("option[value="+ init_date + "]", "#sel_date").prop('selected', true);
}



function searchZodiac(yyyy){
    var remainder = yyyy % 12;
    switch(remainder){
        case 0: return "申（monkey）"; break;
        case 1: return "酉（rooster）"; break;
        case 2: return "戌（dog）"; break;
        case 3: return "亥（boar）"; break;
        case 4: return "子（rat）"; break;
        case 5: return "丑（ox）"; break;
        case 6: return "寅（tiger）"; break;
        case 7: return "卯（rabbit）"; break;
        case 8: return "辰（dragon）"; break;
        case 9: return "巳（snake）"; break;
        case 10: return "午（horse）"; break;
        case 11: return "未（sheep）"; break;
    }
}