// gulp_変数
var gulp = require("gulp");
var sass = require("gulp-sass");
var ex = "expanded";

// ディレクトリ_変数
var sassCommonDir = "./scss/common/*.scss";
var cssCommonDir = "./common/css";

var sassHomeDir = "./scss/*.scss";
var cssHomeDir = "./css";

var sassNewsDir = "./scss/news/*.scss";
var cssNewsDir = "./news/css";

var sassOnairDir = "./scss/onair/*.scss";
var cssOnairDir = "./onair/css";

var sassStoryDir = "./scss/story/*.scss";
var cssStoryDir = "./story/css";

var sassCharacterDir = "./scss/character/*.scss";
var cssCharacterDir = "./character/css";

var sassStaffCastDir = "./scss/staff-cast/*.scss";
var cssStaffCastDir = "./staff-cast/css";

var sassBdDvdDir = "./scss/bd-dvd/*.scss";
var cssBdDvdDir = "./bd-dvd/css";

var sassMovieDir = "./scss/movie/*.scss";
var cssMovieDir = "./movie/css";

var sassMusicDir = "./scss/music/*.scss";
var cssMusicDir = "./music/css";

var sassGoodsDir = "./scss/goods/*.scss";
var cssGoodsDir = "./goods/css";

var sassScienceClubDir = "./scss/science-club/*.scss";
var cssScienceClubDir = "./science-club/css";

var sassSpecialDir = "./scss/special/*.scss";
var cssSpecialDir = "./special/css";

// common
gulp.task("sass_common",function(){
    return gulp.src(sassCommonDir)
    .pipe(sass({
        outputStyle: ex
    }))
    .pipe(gulp.dest(cssCommonDir));
});

// home
gulp.task("sass_home",function(){
    return gulp.src(sassHomeDir)
    .pipe(sass({
        outputStyle: ex
    }))
    .pipe(gulp.dest(cssHomeDir));
});

// news
gulp.task("sass_news",function(){
    return gulp.src(sassNewsDir)
    .pipe(sass({
        outputStyle: ex
    }))
    .pipe(gulp.dest(cssNewsDir));
});

// onair
gulp.task("sass_onair",function(){
    return gulp.src(sassOnairDir)
    .pipe(sass({
        outputStyle: ex
    }))
    .pipe(gulp.dest(cssOnairDir));
});

// story
gulp.task("sass_story",function(){
    return gulp.src(sassStoryDir)
    .pipe(sass({
        outputStyle: ex
    }))
    .pipe(gulp.dest(cssStoryDir));
});

// character
gulp.task("sass_character",function(){
    return gulp.src(sassCharacterDir)
    .pipe(sass({
        outputStyle: ex
    }))
    .pipe(gulp.dest(cssCharacterDir));
});

// staffCast
gulp.task("sass_staffCast",function(){
    return gulp.src(sassStaffCastDir)
    .pipe(sass({
        outputStyle: ex
    }))
    .pipe(gulp.dest(cssStaffCastDir));
});

// bdDvd
gulp.task("sass_bdDvd",function(){
    return gulp.src(sassBdDvdDir)
    .pipe(sass({
        outputStyle: ex
    }))
    .pipe(gulp.dest(cssBdDvdDir));
});

// movie
gulp.task("sass_movie",function(){
    return gulp.src(sassMovieDir)
    .pipe(sass({
        outputStyle: ex
    }))
    .pipe(gulp.dest(cssMovieDir));
});

// music
gulp.task("sass_music",function(){
    return gulp.src(sassMusicDir)
    .pipe(sass({
        outputStyle: ex
    }))
    .pipe(gulp.dest(cssMusicDir));
});

// goods
gulp.task("sass_goods",function(){
    return gulp.src(sassGoodsDir)
    .pipe(sass({
        outputStyle: ex
    }))
    .pipe(gulp.dest(cssGoodsDir));
});

// scienceClub
gulp.task("sass_scienceClub",function(){
    return gulp.src(sassScienceClubDir)
    .pipe(sass({
        outputStyle: ex
    }))
    .pipe(gulp.dest(cssScienceClubDir));
});

// special
gulp.task("sass_special",function(){
    return gulp.src(sassSpecialDir)
    .pipe(sass({
        outputStyle: ex
    }))
    .pipe(gulp.dest(cssSpecialDir));
});







// 監視設定
gulp.task("watch", function(){
    gulp.watch(sassCommonDir, gulp.task("sass_common"));
    gulp.watch(sassHomeDir, gulp.task("sass_home"));
    gulp.watch(sassNewsDir, gulp.task("sass_news"));
    gulp.watch(sassOnairDir, gulp.task("sass_onair"));
    gulp.watch(sassStoryDir, gulp.task("sass_story"));
    gulp.watch(sassCharacterDir, gulp.task("sass_character"));
    gulp.watch(sassStaffCastDir, gulp.task("sass_staffCast"));
    gulp.watch(sassBdDvdDir, gulp.task("sass_bdDvd"));
    gulp.watch(sassMovieDir, gulp.task("sass_movie"));
    gulp.watch(sassMusicDir, gulp.task("sass_music"));
    gulp.watch(sassGoodsDir, gulp.task("sass_goods"));
    gulp.watch(sassScienceClubDir, gulp.task("sass_scienceClub"));
    gulp.watch(sassSpecialDir, gulp.task("sass_special"));
});