// **************************************************
// * ファイル名：./onair/js/base.js                   *
// * 概要     ：onairのjs                            *
// **************************************************
// **************************************************
// * 共通                                            *
// **************************************************
// **************************************************
// * イベント設定                                     *
// **************************************************
window.addEventListener("load", onair_loadMain, false);

// **************************************************
// * 関数                                            *
// **************************************************
// ロードメイン関数
function onair_loadMain(){
    // 初期設定
    // タブ
    const class_tab = "tab_item";
    const ele_tab = document.getElementsByClassName(class_tab);
    var id_target = ele_tab[0].hash.replace("#","");
    addActById(id_target);
    ele_tab[0].classList.add(stts_act);

    // タブ切替
    for(var i=0; i<ele_tab.length; i++){
        ele_tab[i].addEventListener("click",function(event){
            event.preventDefault();
            if(!this.classList.contains(stts_act)){
                // クリックした要素以外に付与された"active"を削除する
                //  + 対応したコンテンツを非表示する
                for(var j=0; j<ele_tab.length; j++){
                    if(ele_tab[j].classList.contains(stts_act)){
                        id_target = ele_tab[j].hash.replace("#","");
                        removeActById(id_target);
                        ele_tab[j].classList.remove(stts_act);
                    }
                }
                // クリックした要素に"active"がない場合、付与する
                // + 対応したコンテンツを表示する
                id_target = this.hash.replace("#","");
                addActById(id_target);
                this.classList.add(stts_act);
            }
        },false);
    }
}