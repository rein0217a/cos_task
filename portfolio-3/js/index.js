// **************************************************
// * ファイル名：./js/index.js                        *
// * 概要     ：homeのjs                             *
// **************************************************
// **************************************************
// * 共通                                            *
// **************************************************
// **************************************************
// * イベント設定                                     *
// **************************************************
window.addEventListener("load", home_loadMain, false);

// **************************************************
// * 関数                                            *
// **************************************************
function home_loadMain(){
    // mainVisualボタン クリック
    const width_mainVisual = window.innerWidth;
    const mainVisuals = document.getElementById("mainVisual_imgs");
    var now_translate = 0;
    // 左ボタン
    const btn_mainVisualLeft = document.getElementById("mainVisual_btns_left");
    btn_mainVisualLeft.addEventListener("click", function(event){
        event.preventDefault();
        if(mainVisuals.style.transform){
            now_translate = parseFloat(mainVisuals.style.transform.split("px")[0].split("(")[1]);
        }
        // 対応する番号を取得
        mainVisuals.style.transform = "translateX(" + (now_translate +(width_mainVisual)) + "px)";
    },false);
    // 右ボタン
    const btn_mainVisualRight = document.getElementById("mainVisual_btns_right");
    btn_mainVisualRight.addEventListener("click", function(event){
        event.preventDefault();
        if(mainVisuals.style.transform){
            now_translate = parseFloat(mainVisuals.style.transform.split("px")[0].split("(")[1]);
        }
        // 対応する番号を取得
        mainVisuals.style.transform = "translateX(" + (now_translate -(width_mainVisual)) + "px)";
    },false);


    // contents アニメーション
    // タッチ開始時
    mainVisuals.addEventListener("touchstart", function(event){
        // 座標取得
        touchStartX = event.touches[0].pageX;
    },false);
    // タッチ移動時
    mainVisuals.addEventListener("touchmove", function(event){
        event.preventDefault();
        // 座標取得
        touchMoveX = event.changedTouches[0].pageX;
        // 現在のtranslete値を取得
        if(this.style.transform){
            now_translate = parseFloat(this.style.transform.split("px")[0].split("(")[1]);
        }
        // 右移動
        if(touchStartX<touchMoveX){
            changeDistance = touchMoveX - touchStartX;
            this.style.transform = "translateX(" + (now_translate + changeDistance) + "px)";
            touchStartX = touchMoveX
        }
        // 左移動
        if(touchMoveX<touchStartX){
            changeDistance = touchStartX - touchMoveX;
            this.style.transform = "translateX(" + (now_translate - changeDistance) + "px)";
            touchStartX = touchMoveX
        }
        // タッチ終了時
        mainVisuals.addEventListener("touchend", function(){
            // 現在のtranslateを取得
            now_translate = parseFloat(this.style.transform.split("px")[0].split("(")[1]);
            var pageNum = Math.round(- now_translate/width_mainVisual);
            // スクロールした結果近かったページを表示
            const maxNum = document.getElementsByClassName("mainVisual_imgs_item").length - 1;
            if(pageNum <= 0){
                this.style.transform = "translateX(" + 0 + "px)";
            }else if(pageNum >= maxNum){
                this.style.transform = "translateX(" + (-(maxNum * width_mainVisual)) + "px)";
            }else{
                this.style.transform = "translateX(" + (-(pageNum * width_mainVisual)) + "px)";
            }
        }, false);
    },false);
}