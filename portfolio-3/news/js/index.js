// **************************************************
// * ファイル名：./news/js/index.js                   *
// * 概要     ：newsのjs                              *
// **************************************************
// **************************************************
// * 共通                                            *
// **************************************************
// **************************************************
// * イベント設定                                     *
// **************************************************
window.addEventListener("load", news_loadMain, false);


// **************************************************
// * 関数                                            *
// **************************************************
// ロードメイン関数
function news_loadMain(){
    // 初期化
    var btn_pageNav = document.getElementsByClassName("pageNav_inner_item");
    btn_pageNav[0].classList.add(stts_act);

    //newsタイトル短縮化
    var ttlLst = document.getElementsByClassName("contents_inner_item_ttl");
    for(var i=0; i < ttlLst.length; i++){
        var str_target = ttlLst[i].textContent;
        var result = strCounter(str_target);
        if(result != 0){
            ttlLst[i].textContent  = ttlLst[i].textContent.substr(0, result) + "…";
        }
    }

    // paggNavクリック後
    for(var i=0; i < btn_pageNav.length; i++){
        btn_pageNav[i].addEventListener("click", function(){
            for(var j=0; j < btn_pageNav.length; j++){
                if(btn_pageNav[j].classList.contains(stts_act)){
                    btn_pageNav[j].classList.remove(stts_act);
                }
            }
            this.classList.add(stts_act);
        }, false);
    }
    // pageNavArrowイベント
    var btn_pageNavArw = document.getElementsByClassName("pageNav_inner_arrow");
    for(var i=0; i<btn_pageNavArw.length; i++){
        const extention = ".svg";
        const txt_hover = "_hover";
        // ホバー
        btn_pageNavArw[i].addEventListener("touchstart", function(ele){
            var txt_src = ele.target.getAttribute("src");
            if(txt_src == null){
                ele.preventDefault;
            }else if(!txt_src.includes(txt_hover)){
                var change_src = txt_src.split(extention)[0] + txt_hover + extention;
                ele.target.setAttribute("src", change_src);
            }
        },false);

        // ホバーアウト
        btn_pageNavArw[i].addEventListener("touchend",function(ele){
            var txt_src = ele.target.getAttribute("src");
            if(txt_src == null){
                ele.preventDefault;
            }else if(txt_src.includes(txt_hover)){
                var change_src = txt_src.replace(txt_hover, "");
                ele.target.setAttribute("src", change_src);
            } 
        }, true);
    }



}

// 全角2とカウント、半角は1とカウントする
function strCounter(str){
    const maxNum = 60;
    var splitNum = 0;
    var count = 0;
    for(var i=0; i< str.length; i++){
        char = str.charCodeAt(i);
        if ((char >= 0x0 && char < 0x81) || (char == 0xf8f0) || (char >= 0xff61 && char < 0xffa0) || (char >= 0xf8f1 && char < 0xf8f4)) {
            count += 1;
        } else {
            count += 2;
        }

        if(count == maxNum){
            splitNum = i;
            break;
        }
    }
    return splitNum;
}