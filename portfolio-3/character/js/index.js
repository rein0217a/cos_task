// **************************************************
// * ファイル名：./character/js/index.js               *
// * 概要     ：characterのjs                        *
// **************************************************
// **************************************************
// * 共通                                            *
// **************************************************
// **************************************************
// * イベント設定                                     *
// **************************************************
window.addEventListener("load", chara_loadMain, false);

// **************************************************
// * 関数                                            *
// **************************************************
function chara_loadMain(){
    // 初期表示
    // characterNav 丸ボタン
    const ele_charaNav_lst = document.getElementsByClassName("character_nav_lst_item");
    ele_charaNav_lst[0].classList.add(stts_act);
    
    
    // characterNav アニメーション
    const width_charaNavInner = 344.5;
    const ele_charaNav = document.getElementById("character_nav_main");
    var now_translate = 0;
    var touchStartX;
    var touchMoveX;
    var changeDistance;
    // タッチ開始時
    ele_charaNav.addEventListener("touchstart", function(event){
        // 座標取得
        touchStartX = event.touches[0].pageX;
    },false);
    // タッチ移動時
    ele_charaNav.addEventListener("touchmove", function(event){
        event.preventDefault();
        // 座標取得
        touchMoveX = event.changedTouches[0].pageX;
        // 現在のtranslete値を取得
        if(this.style.transform){
            now_translate = parseFloat(this.style.transform.split("px")[0].split("(")[1]);
        }
        // 右移動
        if(touchStartX<touchMoveX){
            changeDistance = touchMoveX - touchStartX;
            this.style.transform = "translateX(" + (now_translate + changeDistance) + "px)";
            touchStartX = touchMoveX
        }
        // 左移動
        if(touchMoveX<touchStartX){
            changeDistance = touchStartX - touchMoveX;
            this.style.transform = "translateX(" + (now_translate - changeDistance) + "px)";
            touchStartX = touchMoveX
        }
        // タッチ終了時
        ele_charaNav.addEventListener("touchend", function(){
            // 現在のtranslateを取得
            now_translate = parseFloat(this.style.transform.split("px")[0].split("(")[1]);
            var pageNum = Math.round(- now_translate/width_charaNavInner);
            // スクロールした結果近かったページを表示
            // + メニューの丸ボタンをactiveにする
            for(var i=0; i<ele_charaNav_lst.length; i++){
                ele_charaNav_lst[i].classList.remove(stts_act);
            }
            if(pageNum <= 0){
                this.style.transform = "translateX(" + 0 + "px)";
                ele_charaNav_lst[0].classList.add(stts_act);
            }else if(pageNum >= 4){
                this.style.transform = "translateX(" + (-(4 * width_charaNavInner)) + "px)";
                ele_charaNav_lst[4].classList.add(stts_act);
            }else{
                this.style.transform = "translateX(" + (-(pageNum * width_charaNavInner)) + "px)";
                ele_charaNav_lst[pageNum].classList.add(stts_act);
            }
        }, false);
    },false);


    // characterNav クリック
    const width_contentsBody = window.innerWidth;
    const btn_charaNav = document.getElementsByClassName("character_nav_main_item");
    const ele_contents = document.getElementById("contents_inner");
    for(var i=0; i<btn_charaNav.length; i++){
        btn_charaNav[i].addEventListener("click", function(event){
            event.preventDefault();
            // 対応する番号を取得
            var num_contents = parseInt(this.hash.split("#contents_")[1]) - 1;
            ele_contents.style.transform = "translateX(" + (-(num_contents * width_contentsBody)) + "px)";
        }, false);
    }


    // メニューの丸ボタン クリック
    for(var i=0; i<ele_charaNav_lst.length; i++){
        ele_charaNav_lst[i].addEventListener("click",function(event){
            for(var j=0; j<ele_charaNav_lst.length; j++){
                ele_charaNav_lst[j].classList.remove(stts_act);
            }
            ele_charaNav.style.transform = "translateX(" + (-(event.target.hash.replace("#", "") * width_charaNavInner)) + "px)";
            this.classList.add(stts_act);
        }, false);
    }


    // contentsボタン クリック
    const btn_contentsLeft = document.getElementById("contents_btn_left");
    // 左ボタン
    btn_contentsLeft.addEventListener("click", function(event){
        event.preventDefault();
        if(ele_contents.style.transform){
            now_translate = parseFloat(ele_contents.style.transform.split("px")[0].split("(")[1]);
        }
        // 対応する番号を取得
        ele_contents.style.transform = "translateX(" + (now_translate +(width_contentsBody)) + "px)";
    },false);
    // 右ボタン
    const btn_contentsRight = document.getElementById("contents_btn_right");
    btn_contentsRight.addEventListener("click", function(event){
        event.preventDefault();
        if(ele_contents.style.transform){
            now_translate = parseFloat(ele_contents.style.transform.split("px")[0].split("(")[1]);
        }
        // 対応する番号を取得
        ele_contents.style.transform = "translateX(" + (now_translate -(width_contentsBody)) + "px)";
    },false);


    // contents アニメーション
    // タッチ開始時
    ele_contents.addEventListener("touchstart", function(event){
        // 座標取得
        touchStartX = event.touches[0].pageX;
    },false);
    // タッチ移動時
    ele_contents.addEventListener("touchmove", function(event){
        event.preventDefault();
        // 座標取得
        touchMoveX = event.changedTouches[0].pageX;
        // 現在のtranslete値を取得
        if(this.style.transform){
            now_translate = parseFloat(this.style.transform.split("px")[0].split("(")[1]);
        }
        // 右移動
        if(touchStartX<touchMoveX){
            changeDistance = touchMoveX - touchStartX;
            this.style.transform = "translateX(" + (now_translate + changeDistance) + "px)";
            touchStartX = touchMoveX
        }
        // 左移動
        if(touchMoveX<touchStartX){
            changeDistance = touchStartX - touchMoveX;
            this.style.transform = "translateX(" + (now_translate - changeDistance) + "px)";
            touchStartX = touchMoveX
        }
        // タッチ終了時
        ele_contents.addEventListener("touchend", function(){
            // 現在のtranslateを取得
            now_translate = parseFloat(this.style.transform.split("px")[0].split("(")[1]);
            var pageNum = Math.round(- now_translate/width_contentsBody);
            // スクロールした結果近かったページを表示
            if(pageNum <= 0){
                this.style.transform = "translateX(" + 0 + "px)";
            }else if(pageNum >= 17){
                this.style.transform = "translateX(" + (-(17 * width_contentsBody)) + "px)";
            }else{
                this.style.transform = "translateX(" + (-(pageNum * width_contentsBody)) + "px)";
            }
        }, false);
    },false);
}