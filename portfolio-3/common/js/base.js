// **************************************************
// * ファイル名：./common/js/base.js                  *
// * 概要     ：共通のjs                              *
// **************************************************
// **************************************************
// * 共通                                            *
// **************************************************
// 定数
const stts_def = "default";
const stts_act = "active";

// クラス設定
function addDefById(id){
    document.getElementById(id).classList.add(stts_def);
}
function removeDefById(id){
    document.getElementById(id).classList.remove(stts_def);
}
function addActById(id){
    document.getElementById(id).classList.add(stts_act);
}
function removeActById(id){
    document.getElementById(id).classList.remove(stts_act);
}
// **************************************************
// * イベント設定                                     *
// **************************************************
window.addEventListener("load", loadMain, false);


// **************************************************
// * 関数                                            *
// **************************************************
// ロードメイン関数
function loadMain(){
    // ローディングアニメーション
    loadingAnimation();
    
    // 初期設定
    // ヘッダー
    const id_header = "header";
    addDefById(id_header);

    // headerアニメーション
    const btn_headerNav = document.getElementById("header_inner_btnNav");
    btn_headerNav.addEventListener("click", function(){
        const ele_header = document.getElementById(id_header);
        if(ele_header.classList.contains(stts_def)){
            removeDefById(id_header);
            addActById(id_header);
        }else{
            removeActById(id_header);
            addDefById(id_header);
        }
    });

    // twitterシェアボタン リンク設定
    const btn_twitterShare = document.getElementById("btn_twitterShare");
    btn_twitterShare.href ="https://twitter.com/share?url=" + location.href + "&text=" + document.title.split("｜")[0];

    // YouTube ポップアップ表示
    // YouTube Playerを作成する
    let player;
    function onYouTubePlayerAPIReady() {
        player = new YT.Player('player_main_iframe', {
            height: '360', 
            width: '640'
        });
    }
    onYouTubePlayerAPIReady();
    // YouTube movie動的に変更
    var id_video;
    const linkLst = document.getElementsByClassName("link_movie");
    for(var i=0; i< linkLst.length; i++){
        linkLst[i].addEventListener("click", function(event){
            event.preventDefault();

            // herfの動画idを取得
            id_video = this.href.split('v=')[1];
            // 正しいurlの形式だったとき
            if (id_video) {
              // &=クエリパラーメターがついていることがあるので取り除く
              const ampersandPosition = id_video.indexOf('&');
              if(ampersandPosition != -1) {
                id_video = id_video.substring(0, ampersandPosition);
                }
            }
            // ポップアップ表示
            addActById("player");
            player.cueVideoById({
                videoId: id_video
            });
            player.playVideo();
        }, false);
    }
    // movieポップアップ閉じるボタン
    const btn_movieClose = document.getElementById("player_btnClose");
    btn_movieClose.addEventListener("click", function(event){
        event.preventDefault();
        removeActById("player");
        player.pauseVideo();
    },false);

    // topBack
    var btn_arrowToTop = document.getElementById("footer_arrowToTop");
    var btn_toTop = document.getElementById("footer_btnToTop");
    btn_arrowToTop.addEventListener("click", topBack, false);
    btn_toTop.addEventListener("click", topBack, false);
    function topBack(event){
        event.preventDefault();
        var targetHref = this.hash;
        document.querySelector(targetHref).scrollIntoView({
            behavior: "smooth",
            block: "start"
        });
    }
}

// ローディング関数
function loadingAnimation(){
    var images = document.getElementsByTagName("img");
    var percent = document.getElementById("loading_inner_percent");
    var icon = document.getElementById("loading_inner_icon");
    var icon_img = document.getElementById("loading_inner_percent");
    var count = 0;
    var current;
    
    // 画像の読み込み
    for (var i = 0; i < images.length; i++) {
        var img = new Image(); // 画像の作成
        // 画像読み込み完了したとき
        img.onload = function() {
            count += 1;
        }
        // 画像読み込み失敗したとき
        img.onerror = function() {
            count += 1;
        }
        img.src = images[i].src; // 画像にsrcを指定して読み込み開始
    };
    
    // ローディング処理
    var nowLoading = setInterval(function() {
        // 現在の読み込み具合のパーセントを取得
        current = Math.floor(count / images.length * 100);
        // パーセント表示の書き換え
        percent.innerHTML = current + "%";
        // ゲージの変更
        icon_img.style = "clip-path: inset(" + (100 - current) + "px 0px 0px 0px);"
        // 全て読み込んだ時
        if(count == images.length) {
            // icon完全版
            icon.classList.add("comp");
            // ローディング画面 切替
            setTimeout(function(){
                document.getElementById("loading").classList.add("is_hidden");
                document.getElementById("main").classList.remove("is_hidden");
            }, 2000);
            // ローディングの終了
            clearInterval(nowLoading);
        }
    }, 100);
}