let nowDate = new Date();
let year = nowDate.getFullYear();
let month = nowDate.getMonth() + 1;
let date = nowDate.getDate();

$(document).ready(function(){
    // select初期化
    init_select();

    // sel_monthの値を変更後
    $("#sel_year").change(function(){
        setSelectDate(Number($("#sel_year").val()) ,Number($("#sel_month").val()) ,Number($("#sel_date").val()));
    });
    // sel_yearの値を変更後
    $("#sel_month").change(function(){
        setSelectDate(Number($("#sel_year").val()) ,Number($("#sel_month").val()) ,Number($("#sel_date").val()));
    });

    // btn_search押下後
    $("#btn_search").on("click", function(){
        var result = searchConstellation(Number($("#sel_month").val()) ,Number($("#sel_date").val()))　;
        $(".result").text(result);
    });
});



// select初期化関数
function init_select(){
    // sel_year初期化
    for(var i = year; i >= 1920; i--){
        $("#sel_year").append('<option value="' + i + '">' + i + '</option>');
    }
    $("option[value="+ (year-20) + "]", "#sel_year").prop('selected', true);

    // sel_month初期化
    for(var i = 1; i <= 12; i++){
        $("#sel_month").append('<option value="' + i + '">' + i + '</option>');
    }
    $("option[value="+ month + "]", "#sel_month").prop('selected', true);

    // sel_date初期化
    setSelectDate(year ,month ,date);
}



// select_Date設定関数
function setSelectDate(init_year ,init_month, init_date){
    if($("#sel_date").val()){
        $("#sel_date > option").remove();
    }
    var maxDate;
    switch(init_month){
        case 1:
        case 3:
        case 5:
        case 7:
        case 8:
        case 10:
        case 12:
            maxDate = 31;
            break;
        case 4:
        case 6:
        case 9:
        case 11:
            maxDate = 30;
            break;
        case 2:
            if(init_year%4 == 0){
                maxDate = 29;
            }else{
                maxDate = 28;
            }
            break;
    }
    for(var i = 1; i <= maxDate; i++){
        $("#sel_date").append('<option value="' + i + '">' + i + '</option>');
    }
    $("option[value="+ init_date + "]", "#sel_date").prop('selected', true);
}



function searchConstellation(mm, dd){
    let constellationLst = [
        ["牡羊座（Aries）", 3, 21, 4, 19],
        ["牡牛座（Taurus）", 4, 20, 5, 20],	
        ["双子座（Gemini）", 5, 21, 6, 21],
        ["蟹座（Cancer）", 6, 22, 7, 22],
        ["獅子座（Leo）", 7, 23, 8, 22],
        ["乙女座（Virgo）", 8, 23, 9, 22],	
        ["天秤座（Libra）", 9, 23, 10, 23],
        ["蠍座（Scorpio）",	10, 24, 11, 22],		
        ["射手座（Sagittarius）", 11, 23, 12, 21],		
        ["山羊座（Capricorn）",	12, 22, 1, 20],
        ["水瓶座（Aquarius）",1, 21, 2, 18],	
        ["魚座（Pisces）", 2, 19, 3, 20]
    ]
    
    for(var i = 0; i < constellationLst.length; i++){
        if(constellationLst[i][1] == mm){
            if(constellationLst[i][2] <= dd){
                return constellationLst[i][0];
                break;
            }
        }else if(constellationLst[i][3] == mm){
            if(constellationLst[i][4] >= dd){
                return constellationLst[i][0];
                break;
            }
        }
    }
}