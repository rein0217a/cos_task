let cls_num = ".bar_num";
let cls_back = ".bar_back";

let txt_red = "is-red";
let txt_ylw = "is-yellow"
let txt_gen = "is-green";

let txt_width = "width";

$(document).ready(function(){
    // 初期表示
    $(cls_num).text("10");
    $(cls_back).addClass(txt_red);
    $(cls_back).css(txt_width, "10%");

    // マイナスボタン押下
    $(".btn_minus").on("click", function(ev){
        ev.preventDefault();
        var num = Number($(cls_num).text());
        if(num != 0){
            num -= 1;
            $(cls_num).text(num);
            $(cls_back).css(txt_width, num + "%");
            setColorClass(num);
        }
    });

    // プラスボタン押下
    $(".btn_plus").on("click", function(ev){
        ev.preventDefault();
        var num = Number($(cls_num).text());
        if(num != 100){
            num += 1;
            $(cls_num).text(num);
            $(cls_back).css(txt_width, num + "%");
            setColorClass(num);
        }
    });
});

function setColorClass(num){
    $(cls_back).removeClass(function(index, className){
        return (className.match(/\bis-\S+/g) || []).join(' ');
    });
    if(num < 20){
        $(cls_back).addClass(txt_red);
    }else if ((num >= 20) && (num < 50)){
        $(cls_back).addClass(txt_ylw);
    }else if(num >= 50){
        $(cls_back).addClass(txt_gen);
    }
}