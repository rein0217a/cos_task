// **************************************************
// * ファイル名：./common/js/base.js                  *
// * 概要     ：共通のjs                              *
// **************************************************
// **************************************************
// * 共通                                            *
// **************************************************
// class設定
function addDefault(id){
    document.getElementById(id).classList.add("default");
}
function removeDefault(id){
    document.getElementById(id).classList.remove("default");
}
function addActive(id){
    document.getElementById(id).classList.add("active");
}
function removeActive(id){
    document.getElementById(id).classList.remove("active");
}
// 引数の要素のページ上からの座標を取得
function contentDistance(ele){
    var clientRect = ele.getBoundingClientRect();
    return window.pageYOffset + clientRect.top;
}
// **************************************************
// * ローディング画面表示／非表示                        *
// **************************************************
function loadingAnimation(){
    var hide = "hidden";
    document.getElementById("contents").classList.remove(hide);
    document.getElementById("loading").classList.add(hide);
}

// **************************************************
// * ヘッダーメニューアニメーション                      *
// **************************************************
function headerNavOn(){
    var id_nav = "headerNav";
    var id_btn = "btnNav";
    var def = "default";
    var headerNavClassLst = document.getElementById(id_nav).classList;

    if(headerNavClassLst.contains(def)){
        removeDefault(id_nav);
        addActive(id_nav);
        addActive(id_btn);
    }
}
function headerNavOff(){
    var id_nav = "headerNav";
    var id_btn = "btnNav";
    var act = "active";
    var headerNavClassLst = document.getElementById(id_nav).classList;

    if(headerNavClassLst.contains(act)){
        removeActive(id_nav);
        removeActive(id_btn);
        addDefault(id_nav);
    }
}
// **************************************************
// * スムーズスクロール                                *
// * クリックイベントで使用すること                      *
// **************************************************
function smoothScroll(ele){
    for(var i=0; i < ele.path.length; i++){
        // element要素がaの場合
        if(ele.path[i].localName == "a"){
            var targetHref = ele.path[i].hash;
            // hrefの値に「#」が含まれている場合
            if(targetHref.includes("#")){
                ele.preventDefault();
                document.querySelector(targetHref).scrollIntoView({
                    behavior: "smooth",
                    block: "start"
                });
                break;
            }else{
                break;
            }
        }
    }
}