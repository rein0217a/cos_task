// gulp_変数
var gulp = require("gulp");
var sass = require("gulp-sass");
var ex = "expanded";

// ディレクトリ_変数
var sassCommonDir = "./scss/common/*.scss";
var cssCommonDir = "./common/css";
var sassHomeDir = "./scss/css/*.scss";
var cssHomeDir = "./css";
var sassNewsDir = "./scss/news/*.scss";
var cssNewsDir = "./news/css";
var sassGreetingDir = "./scss/greeting/*.scss";
var cssGreetingDir = "./greeting/css";
var sassOutlineDir = "./scss/outline/*.scss";
var cssOutlineDir = "./outline/css";
var sassBrandingDir = "./scss/branding/*.scss";
var cssBrandingDir = "./branding/css";
var sassRecruitDir = "./scss/recruit/*.scss";
var cssRecruitDir = "./recruit/css";


// common
gulp.task("sass_common",function(){
    return gulp.src(sassCommonDir)
    .pipe(sass({
        outputStyle: ex
    }))
    .pipe(gulp.dest(cssCommonDir));
});

// home
gulp.task("sass_home",function(){
    return gulp.src(sassHomeDir)
    .pipe(sass({
        outputStyle: ex
    }))
    .pipe(gulp.dest(cssHomeDir));
});

// news
gulp.task("sass_news",function(){
    return gulp.src(sassNewsDir)
    .pipe(sass({
        outputStyle: ex
    }))
    .pipe(gulp.dest(cssNewsDir));
});

// greeting
gulp.task("sass_greeting",function(){
    return gulp.src(sassGreetingDir)
    .pipe(sass({
        outputStyle: ex
    }))
    .pipe(gulp.dest(cssGreetingDir));
});

// outline
gulp.task("sass_outline",function(){
    return gulp.src(sassOutlineDir)
    .pipe(sass({
        outputStyle: ex
    }))
    .pipe(gulp.dest(cssOutlineDir));
});

// branding
gulp.task("sass_branding",function(){
    return gulp.src(sassBrandingDir)
    .pipe(sass({
        outputStyle: ex
    }))
    .pipe(gulp.dest(cssBrandingDir));
});

// recruit
gulp.task("sass_recruit",function(){
    return gulp.src(sassRecruitDir)
    .pipe(sass({
        outputStyle: ex
    }))
    .pipe(gulp.dest(cssRecruitDir));
});


// 監視設定
gulp.task("watch", function(){
    gulp.watch(sassCommonDir, gulp.task("sass_common"));
    gulp.watch(sassHomeDir, gulp.task("sass_home"));
    gulp.watch(sassNewsDir, gulp.task("sass_news"));
    gulp.watch(sassGreetingDir, gulp.task("sass_greeting"));
    gulp.watch(sassOutlineDir, gulp.task("sass_outline"));
    gulp.watch(sassBrandingDir, gulp.task("sass_branding"));
    gulp.watch(sassRecruitDir, gulp.task("sass_recruit"));
});