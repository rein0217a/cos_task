window.onload = main;
document.onclick = onclickMain;
document.onmouseenter = onmouseenterMain;
document.onmouseleave = onmouseleaveMain;
window.onscroll = scrollMain;

// **************************************************
// * ロードイベント                                   *
// **************************************************
function main(){
    // テキストの文字数超過している場合「…」を付与する
    longTxtChange("item_ttl", 17);
    longTxtChange("item_txt", 63);
    // ローディング画面
    loadingAnimation();
    // ヘッダーメニュー非表示
    addDefault("headerNav");

    // 新着情報を表示／非表示
    var itemWrapLst = document.getElementsByClassName("itemWrap");
    for(var i = 0; i < itemWrapLst.length; i++){
        if(i == 0){
            itemWrapLst[i].classList.add("active");
        }else{
            itemWrapLst[i].classList.add("default");
        }
    }
}
// **************************************************
// * クリックイベント                                  *
// **************************************************
function onclickMain(ele){
    // スムーズスクロール設定
    smoothScroll(ele);
}
// **************************************************
// * ホバーイベント                                   *
// **************************************************
function onmouseenterMain(ele){
    // ヘッダーメニュー表示
    document.getElementById("btnNav").onmouseenter = headerNavOn;
}
function onmouseleaveMain(ele){
    // ヘッダーメニュー非表示
    document.getElementById("headerNav").onmouseleave = headerNavOff;
}
// **************************************************
// * スクロールイベント                                *
// **************************************************
function scrollMain(){
    var nowPoint = window.pageYOffset;

    // 新着情報をスクロール時表示する
    itemWrapAnimation(nowPoint);
}
// **************************************************
// * 関数                                            *
// **************************************************
// テキストの文字数超過している場合「…」を付与する
function longTxtChange(className, maxNum){
    // ニュースタイトルのテキスト変換
    var targetLst = document.getElementsByClassName(className);

    for(var i = 0; i< targetLst.length; i++){
        var txtOrg =  targetLst[i].textContent;
        if (txtOrg.length > maxNum){
            targetLst[i].textContent  = targetLst[i].textContent.substr(0, maxNum-1) + "…";
        }
    }
    
}
// 新着情報をスクロール時表示する
function itemWrapAnimation(nowPoint){
    var itemWrapLst = document.getElementsByClassName("itemWrap");
    var breakPointLst = new Array();
    for(var i = 0; i < itemWrapLst.length; i++){
        breakPointLst.push(contentDistance(itemWrapLst[i]) - 400);
    }
    for(var i = 0; i < itemWrapLst.length; i++){
        if(nowPoint > breakPointLst[i]){
            itemWrapLst[i].classList.add("active");
        }
    }
}
