window.onload = main;
document.onclick = onclickMain;
document.onmouseenter = onmouseenterMain;
document.onmouseleave = onmouseleaveMain;
window.onscroll = scrollMain;

// **************************************************
// * ロードイベント                                   *
// **************************************************
function main(){
    // ローディング画面
    loadingAnimation();
    // ヘッダーメニュー非表示
    addDefault("headerNav");
    // greetingアニメーション
    addActive("greeting");
    // その他コンテンツ非アニメーション
    addDefault("philosophy");
    addDefault("managementPolicy");
    addDefault("coporateVision");
    addDefault("philosophy_contents");
    addDefault("managementPolicy_contents");
    addDefault("coporateVision_contents");
}
// **************************************************
// * クリックイベント                                  *
// **************************************************
function onclickMain(ele){
    // スムーズスクロール設定
    smoothScroll(ele);
}
// **************************************************
// * ホバーイベント                                   *
// **************************************************
function onmouseenterMain(ele){
    // ヘッダーメニュー表示
    document.getElementById("btnNav").onmouseenter = headerNavOn;
}
function onmouseleaveMain(ele){
    // ヘッダーメニュー非表示
    document.getElementById("headerNav").onmouseleave = headerNavOff;
}
// **************************************************
// * スクロールイベント                                *
// **************************************************
function scrollMain(){
    var nowPoint = window.pageYOffset;

    // コンテンツアニメーション
    var breakPoint_philosophy = contentDistance(document.getElementById("philosophy")) - window.innerHeight/2;
    var breakPoint_policy = contentDistance(document.getElementById("managementPolicy")) - window.innerHeight/2;
    var breakPoint_vision = contentDistance(document.getElementById("coporateVision")) - window.innerHeight/2;
    if(nowPoint > breakPoint_philosophy){
        addActive("philosophy");
        addActive("philosophy_contents");
        addActive("managementPolicy_contents");
        addActive("coporateVision_contents");
    }
    if(nowPoint > breakPoint_policy){
        addActive("managementPolicy");
    }
    if(nowPoint > breakPoint_vision){
        addActive("coporateVision");
    }
}
// **************************************************
// * 関数                                            *
// **************************************************
