window.onload = main;
document.onclick = onclickMain;
document.onmouseenter = onmouseenterMain;
document.onmouseleave = onmouseleaveMain;
window.onscroll = scrollMain;

// **************************************************
// * ロードイベント                                   *
// **************************************************
function main(){

    // ローディング画面
    loadingAnimation();
    // ヘッダーメニュー非表示
    addDefault("headerNav");
    // メインビジュアル初期設定
    addActive("mainVisual");
    // タイトル初期設定
    var sectionTtlLst = document.getElementsByClassName("section_ttl");
    for(var i=0; i<sectionTtlLst.length; i++){
        sectionTtlLst[i].classList.add("default");
    }
    // コンテンツ初期設定
    var sectionLst = document.getElementsByTagName("section");
    for(var i=0; i<sectionLst.length; i++){
        sectionLst[i].classList.add("default");
    }
}
// **************************************************
// * クリックイベント                                  *
// **************************************************
function onclickMain(ele){
    // スムーズスクロール設定
    smoothScroll(ele);
}
// **************************************************
// * ホバーイベント                                   *
// **************************************************
function onmouseenterMain(ele){
    // ヘッダーメニュー表示
    document.getElementById("btnNav").onmouseenter = headerNavOn;
}
function onmouseleaveMain(ele){
    // ヘッダーメニュー非表示
    document.getElementById("headerNav").onmouseleave = headerNavOff;
}
// **************************************************
// * スクロールイベント                                *
// **************************************************
function scrollMain(){
    var nowPoint = window.pageYOffset;

    // sectionタイトルスクロールアニメーション
    ttlScrolAnimation(nowPoint);
    // sectionタイトルアニメーション
    sectionTtlAnimation(nowPoint);
    // コンテンツアニメーション
    contentsAnimation(nowPoint);
}
// **************************************************
// * 関数                                            *
// **************************************************
// sectionタイトルスクロールアニメーション
function ttlScrolAnimation(nowPoint){
    var nowBottomPoint = nowPoint + document.documentElement.clientHeight;
    var ttlHeight = 450;
    var newsTtl = document.getElementById("news_ttl");
    var breakPoint_news = contentDistance(document.getElementById("news")) + 200;
    var greetingTtl = document.getElementById("greeting_ttl");
    var breakPoint_greeting = contentDistance(document.getElementById("greeting")) + 200;
    var outlineTtl = document.getElementById("outline_ttl");
    var breakPoint_outline = contentDistance(document.getElementById("outline")) + 200;
    var brandingTtl = document.getElementById("branding_ttl");
    var breakPoint_branding = contentDistance(document.getElementById("branding")) + 200;
    var recruitTtl = document.getElementById("recruit_ttl");
    var breakPoint_recruit = contentDistance(document.getElementById("recruit")) + 200;

    function setTranslateY(breakPoint, targetTtl){
        if((nowBottomPoint > breakPoint)){
            var diff = nowBottomPoint - breakPoint;
            if(diff <= ttlHeight){
                targetTtl.style.webkitTransform = "translateY(" + -(diff) + "px)";
                targetTtl.style.transform = "translateY(" + -(diff) + "px)";
            }else{
                targetTtl.style.webkitTransform = "translateY(" + -(ttlHeight) + "px)";
                targetTtl.style.transform = "translateY(" + -(ttlHeight) + "px)";
            }
        }else{
            targetTtl.style.webkitTransform = "";
            targetTtl.style.transform = "";
        }
    }
    setTranslateY(breakPoint_news, newsTtl);
    setTranslateY(breakPoint_greeting, greetingTtl);
    setTranslateY(breakPoint_outline, outlineTtl);
    setTranslateY(breakPoint_branding, brandingTtl);
    setTranslateY(breakPoint_recruit, recruitTtl);
}
// sectionタイトルアニメーション
function sectionTtlAnimation(nowPoint){
    var breakPoint_newsTtl = contentDistance(document.getElementById("news_ttl")) - window.innerHeight/2;
    var breakPoint_greetingTtl = contentDistance(document.getElementById("greeting_ttl")) - window.innerHeight/2;
    var breakPoint_outlineTtl = contentDistance(document.getElementById("outline_ttl")) - window.innerHeight/2;
    var breakPoint_brandingTtl = contentDistance(document.getElementById("branding_ttl")) - window.innerHeight/2;
    var breakPoint_recruitTtl = contentDistance(document.getElementById("recruit_ttl")) - window.innerHeight/2;

    if(nowPoint > breakPoint_newsTtl){
        removeDefault("news_ttl");
        addActive("news_ttl");
    }
    if(nowPoint > breakPoint_greetingTtl){
        removeDefault("greeting_ttl");
        addActive("greeting_ttl");
    }
    if(nowPoint > breakPoint_outlineTtl){
        removeDefault("outline_ttl");
        addActive("outline_ttl");
    }
    if(nowPoint > breakPoint_brandingTtl){
        removeDefault("branding_ttl");
        addActive("branding_ttl");
    }
    if(nowPoint > breakPoint_recruitTtl){
        removeDefault("recruit_ttl");
        addActive("recruit_ttl");
    }
}

// コンテンツアニメーション 
function contentsAnimation(nowPoint){
    var breakPoint_news = contentDistance(document.getElementById("news")) - window.innerHeight/4;
    var breakPoint_greeting = contentDistance(document.getElementById("greeting")) - window.innerHeight/4;
    var breakPoint_outline = contentDistance(document.getElementById("outline")) - window.innerHeight/4;
    var breakPoint_branding = contentDistance(document.getElementById("branding")) - window.innerHeight/4;
    var breakPoint_recruit = contentDistance(document.getElementById("recruit")) - window.innerHeight/4;
    
    if(nowPoint > breakPoint_news){
        removeDefault("news");
        addActive("news");
    }
    if(nowPoint > breakPoint_greeting){
        removeDefault("greeting");
        addActive("greeting");
    }
    if(nowPoint > breakPoint_outline){
        removeDefault("outline");
        addActive("outline");
    }
    if(nowPoint > breakPoint_branding){
        removeDefault("branding");
        addActive("branding");
    }
    if(nowPoint > breakPoint_recruit){
        removeDefault("recruit");
        addActive("recruit");
    }
}