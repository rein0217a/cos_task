window.onload = main;
document.onclick = onclickMain;
document.onmouseenter = onmouseenterMain;
document.onmouseleave = onmouseleaveMain;
window.onscroll = scrollMain;

// **************************************************
// * ロードイベント                                   *
// **************************************************
function main(){
    // ローディング画面
    loadingAnimation();
    // ヘッダーメニュー非表示
    addDefault("headerNav");
    // serviceアニメーション
    addActive("service");
    // その他コンテンツ非アニメーション
    addDefault("domain");
    addDefault("shops");
    // ショップリスト初期設定
    addActive("shopList01");
    // ショップ詳細初期設定
    addActive("shop01");
    var shopsDetailLst = document.getElementsByClassName("shops_contents_shopDetail_item");
    for(var i = 0; i < shopsDetailLst.length; i++){
        shopsDetailLst[i].classList.add("default");
    }
}
// **************************************************
// * クリックイベント                                  *
// **************************************************
function onclickMain(ele){
    // スムーズスクロール設定
    smoothScroll(ele);

    shopsMapAnimation(ele);
}
// **************************************************
// * ホバーイベント                                   *
// **************************************************
function onmouseenterMain(ele){
    // ヘッダーメニュー表示
    document.getElementById("btnNav").onmouseenter = headerNavOn;
}
function onmouseleaveMain(ele){
    // ヘッダーメニュー非表示
    document.getElementById("headerNav").onmouseleave = headerNavOff;
}
// **************************************************
// * スクロールイベント                                *
// **************************************************
function scrollMain(){
    var nowPoint = window.pageYOffset;

    // コンテンツアニメーション
    var breakPoint_history = contentDistance(document.getElementById("domain")) - window.innerHeight/2;
    var breakPoint_chart = contentDistance(document.getElementById("shops")) - window.innerHeight/2;
    if(nowPoint > breakPoint_history){
        if(!document.getElementById("domain").classList.contains("active")){
            domainContentAnimation();
        }
        removeDefault("domain");
        addActive("domain");
    }
    if(nowPoint > breakPoint_chart){
        removeDefault("shops");
        addActive("shops");
    }
}
// **************************************************
// * 関数                                            *
// **************************************************
// ドメインコンテンツアニメーション
function domainContentAnimation(){
    var contentsLst = document.getElementsByClassName("domain_contents_item");
    var nowNum = 0;
    var nextNum = nowNum+1;

    contentsLst[nowNum].classList.add("active");
    // 5秒後
    setTimeout(() => {
        contentsLst[nowNum].classList.remove("active");
        contentsLst[nextNum].classList.add("active");
        nowNum = nextNum;
        // 3秒毎に実行
        setInterval(() => {
            switch(nowNum){
                case 0:
                case 1:
                case 2:
                    nextNum = nowNum+1;
                    break;
                case 3:
                    nextNum = 7;
                    break;
                case 4:
                    nextNum = 0;
                    break;
                case 5:
                case 6:
                case 7:
                    nextNum = nowNum-1;
                    break;
            }
            contentsLst[nowNum].classList.remove("active");
            contentsLst[nextNum].classList.add("active");
            nowNum = nextNum;
        }, 3000);
    }, 5000);
}
// ショップマップアニメーション
function shopsMapAnimation(ele){
    for(var i=0; i < ele.path.length; i++){
        // element要素がaの場合
        if(ele.path[i].localName == "a"){
            if(ele.path[i].id.includes("shopList")){
                ele.preventDefault();
                // 既にactiveになっているものを変更する
                var nowEle = document.getElementsByClassName("shops_contents_shopList_btn active")[0];
                var nowAnchor = document.getElementById(nowEle.title);
                var nowShopDetail = document.getElementById("shop" + nowEle.id.slice(-2));
                nowEle.classList.remove("active");
                nowAnchor.classList.remove("active");

                // 全てのアンカーに一度defaultを付与する。
                var anchorLst = document.getElementsByClassName("shops_contents_map_anchor");
                for(var j=0; j < anchorLst.length; j++){
                    anchorLst[j].classList.add("default");
                }
                nowShopDetail.classList.remove("active");
                nowShopDetail.classList.add("default");

                // ボタン押下されたものに対応するアイテムをactiveにする
                var targetEle = document.getElementById(ele.path[i].id);
                var targetAnchor = document.getElementById(targetEle.title);
                var targetLstNum = document.getElementById("shop" + targetEle.id.slice(-2));
                targetEle.classList.add("active");
                targetAnchor.classList.add("active");
                targetAnchor.classList.remove("default");
                targetLstNum.classList.add("active");
                targetLstNum.classList.remove("default");
                break;
                
            }else{
                break;
            }
        }
    }
}