window.onload = main;
document.onclick = onclickMain;
document.onmouseenter = onmouseenterMain;
document.onmouseleave = onmouseleaveMain;
window.onscroll = scrollMain;

// **************************************************
// * ロードイベント                                   *
// **************************************************
function main(){
    // ローディング画面
    loadingAnimation();
    // ヘッダーメニュー非表示
    addDefault("headerNav");
    // outlineアニメーション
    addActive("outline");
    // その他コンテンツ非アニメーション
    addDefault("history");
    addDefault("chart");

    // ロゴに込めた思いbox表示／非表示
    document.getElementById("aboutLogo_btn").onclick = function(ele){
        ele.preventDefault();
        removeDefault("aboutLogoBox");
        addActive("aboutLogoBox");
    }
    document.getElementById("aboutLogoBox_btnClose").onclick = function(ele){
        ele.preventDefault();
            removeActive("aboutLogoBox");
            addDefault("aboutLogoBox");
    }
}
// **************************************************
// * クリックイベント                                  *
// **************************************************
function onclickMain(ele){
    // スムーズスクロール設定
    smoothScroll(ele);
}
// **************************************************
// * ホバーイベント                                   *
// **************************************************
function onmouseenterMain(ele){
    // ヘッダーメニュー表示
    document.getElementById("btnNav").onmouseenter = headerNavOn;
}
function onmouseleaveMain(ele){
    // ヘッダーメニュー非表示
    document.getElementById("headerNav").onmouseleave = headerNavOff;
}
// **************************************************
// * スクロールイベント                                *
// **************************************************
function scrollMain(){
    var nowPoint = window.pageYOffset;

    // コンテンツアニメーション
    var breakPoint_history = contentDistance(document.getElementById("history")) - window.innerHeight/2;
    var breakPoint_chart = contentDistance(document.getElementById("chart")) - window.innerHeight/2;
    if(nowPoint > breakPoint_history){
        removeDefault("history");
        addActive("history");
    }
    if(nowPoint > breakPoint_chart){
        removeDefault("chart");
        addActive("chart");
    }
}
// **************************************************
// * 関数                                            *
// **************************************************