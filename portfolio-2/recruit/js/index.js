window.onload = main;
document.onclick = onclickMain;
document.onmouseenter = onmouseenterMain;
document.onmouseleave = onmouseleaveMain;
window.onscroll = scrollMain;

// **************************************************
// * ロードイベント                                   *
// **************************************************
function main(){
    // ローディング画面
    loadingAnimation();
    // ヘッダーメニュー非表示
    addDefault("headerNav");
    // その他コンテンツ非アニメーション
    addDefault("eigyou");
    addDefault("seibi");
    addDefault("front");
    addDefault("customer");
    addDefault("kanri");
    addDefault("hokenbu");
}
// **************************************************
// * クリックイベント                                  *
// **************************************************
function onclickMain(ele){
    // スムーズスクロール設定
    smoothScroll(ele);
}
// **************************************************
// * ホバーイベント                                   *
// **************************************************
function onmouseenterMain(ele){
    // ヘッダーメニュー表示
    document.getElementById("btnNav").onmouseenter = headerNavOn;
}
function onmouseleaveMain(ele){
    // ヘッダーメニュー非表示
    document.getElementById("headerNav").onmouseleave = headerNavOff;
}
// **************************************************
// * スクロールイベント                                *
// **************************************************
function scrollMain(){
    var nowPoint = window.pageYOffset;

    // コンテンツアニメーション
    var breakPoint_eigyou = contentDistance(document.getElementById("eigyou")) - window.innerHeight/2;
    var breakPoint_seibi = contentDistance(document.getElementById("seibi")) - window.innerHeight/2;
    var breakPoint_front = contentDistance(document.getElementById("front")) - window.innerHeight/2;
    var breakPoint_customer = contentDistance(document.getElementById("customer")) - window.innerHeight/2;
    var breakPoint_kanri = contentDistance(document.getElementById("kanri")) - window.innerHeight/2;
    var breakPoint_hokenbu = contentDistance(document.getElementById("hokenbu")) - window.innerHeight/2;
    if(nowPoint > breakPoint_eigyou){
        removeDefault("eigyou");
        addActive("eigyou");
    }
    if(nowPoint > breakPoint_seibi){
        removeDefault("seibi");
        addActive("seibi");
    }
    if(nowPoint > breakPoint_front){
        removeDefault("front");
        addActive("front");
    }
    if(nowPoint > breakPoint_customer){
        removeDefault("customer");
        addActive("customer");
    }
    if(nowPoint > breakPoint_kanri){
        removeDefault("kanri");
        addActive("kanri");
    }
    if(nowPoint > breakPoint_hokenbu){
        removeDefault("hokenbu");
        addActive("hokenbu");
    }
}
// **************************************************
// * 関数                                            *
// **************************************************