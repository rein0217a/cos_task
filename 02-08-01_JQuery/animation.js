$(document).ready(function(){
    // 要素ドラッグした時
    $(".dragArea").on("drop", function(event){
        event.preventDefault();
        // メッセージ出力
        var file = event.originalEvent.dataTransfer.files[0];
        if(file == null){
            $(".status").text("");
        }else{
            $(".status").text(file.name +"をアップロードしました。");
        }
    });

    // ドラッグエリア外にドロップした場合のクリア
    $(document).on("dragover", function(event){
        event.preventDefault();
    });
    $(document).on("drop", function(event){
        event.preventDefault();
    });
});