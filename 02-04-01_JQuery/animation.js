$(document).ready(function(){
    
    modalHide();
    $(".btn_opn").on("click", modalShow);
    $(".btn_close").on("click", modalHide);

});

function modalHide(){
    $(".modal").hide();
    $(".bg").hide();
}
function modalShow(){
    $(".modal").show();
    $(".bg").show();
}