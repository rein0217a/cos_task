$(document).ready(function(){
    setInterval(clock, 1000);
    setInterval(delimiter, 500);
});

// 時計関数
function clock(){
    var sel_time = ".clock_time";
    var sel_min = ".clock_minute";
    var sel_sec = ".clock_second";
    
    // 現在時刻を取得
    var nowDate = new Date();
    var hour = nowDate.getHours().toString();
    var min = nowDate.getMinutes().toString();
    var sec = nowDate.getSeconds().toString();

    // 時間を設定
    settingClass(sel_time, hour);
    // 分を設定
    settingClass(sel_min, min);
    // 秒を設定
    settingClass(sel_sec, sec);
}
// 区切り関数
function delimiter(){
    $(".clock_delimiter_item").toggleClass("is-active");
}




// クラスを設定する（分岐含む）
// -引数-
// targetCls: ターゲットクラス
// targetStr: ターゲットの文字列
function settingClass(targetCls, targetStr){
    var sel_tPlc = ".tensPlc";
    var sel_oPlc = ".onesPlc";

    if(targetStr.length == 1){
        addClassNum(targetCls, sel_tPlc, "0");
        addClassNum(targetCls, sel_oPlc, targetStr);
    } else{
        addClassNum(targetCls, sel_tPlc, targetStr.charAt(0));
        addClassNum(targetCls, sel_oPlc, targetStr.charAt(1));
    };
}

// 数字別クラス追加関数（不要クラス削除含む）
// -引数-
// hms  : sel_time or sel_min or sel_sec
// place: sel_tPlc or sel_oPlc
// str  : 設定したい数字文字
function addClassNum(hms ,place, str){
    
    // 既に設定されている数字クラスを削除
    $(hms).children(place).removeClass(
        function(index, className){
            return (className.match(/\bnum-\S+/g) || []).join(' ');
    });
    // 対象のクラスを追加
    var assCls = "num-" + str;
    $(hms).children(place).addClass(assCls);
}