let asc = "ascend";
let des = "desend";

let tbody = "tbody";
let tr = "tr";

$(document).ready(function(){
    // select初期化
    initSelect();
    // table初期化
    initTable();

    // ソート順変更後
    $("#sel_sort").change(function(){
        switch($("#sel_sort option:selected").val()){
            case "0":
                $(tbody).html($(tr, tbody).sort(compareIdAsc));
                break;
            case "1":
                $(tbody).html($(tr, tbody).sort(compareIdDes));
                break;
            case "2":
                $(tbody).html($(tr, tbody).sort(compareJaAsc));
                break;
            case "3":
                $(tbody).html($(tr, tbody).sort(compareJaDes));
                break;
            case "4":
                $(tbody).html($(tr, tbody).sort(compareLaAsc));
                break;
            case "5":
                $(tbody).html($(tr, tbody).sort(compareLaDes));
                break;
        }
    });
});


// select初期化関数
function initSelect(){
    let sortLst = [
        "IDで昇順",
        "IDで降順",
        "Japaneseで昇順",
        "Japameseで降順",
        "Latinで昇順",
        "Latinで降順"
    ]

    for(var i = 0; i < sortLst.length; i++){
        var str = '<option value="';
        str += i;
        str += '">';
        str += sortLst[i];
        str += '</option>';
        $("#sel_sort").append(str);
    }
}

// table初期化関数
function initTable(){
    let constellationLst = [
        ["牡羊座", "Aries"],
        ["牡牛座", "Taurus"],
        ["双子座", "Gemini"],
        ["蟹座", "Cancer"],
        ["獅子座", "Leo"],
        ["乙女座", "Virgo"],
        ["天秤座", "Libra"],
        ["蠍座", "Scorpio"],
        ["射手座", "Sagittarius"],
        ["山羊座", "Capricorn"],
        ["水瓶座", "Aquarius"],
        ["魚座", "Pisces"]
    ]
    
    for(var i = 0; i < constellationLst.length; i++){
        var str = "<tr>";
        str += "<td>";
        str += i + 1;
        str += "</td>";
        for(var j = 0; j < 2; j++){
            str += "<td>"
            str += constellationLst[i][j];
            str += "</td>";
        }
        str += "</tr>";
        $(tbody ,".info").append(str);
    }
}


// ID_sort関数
let kanjiLst = [
    "射手座",
    "魚座",
    "牡牛座",
    "乙女座",
    "牡羊座",
    "蟹座",
    "蠍座",
    "獅子座",
    "天秤座",
    "双子座",
    "水瓶座",
    "山羊座"
]
function compareIdAsc(a, b){
    var a_num = Number($(a).find('td').eq(0).text());
    var b_num = Number($(b).find('td').eq(0).text());

    return a_num - b_num;
}
function compareIdDes(a, b){
    var a_num = Number($(a).find('td').eq(0).text());
    var b_num = Number($(b).find('td').eq(0).text());

    return b_num - a_num;
}

// Japanese_sort関数
function compareJaAsc(a, b){
    var a_index = kanjiLst.indexOf($(a).find('td').eq(1).text());
    var b_index = kanjiLst.indexOf($(b).find('td').eq(1).text());
    
    return a_index - b_index;
}
function compareJaDes(a, b){
    var a_index = kanjiLst.indexOf($(a).find('td').eq(1).text());
    var b_index = kanjiLst.indexOf($(b).find('td').eq(1).text());
    
    return b_index - a_index;
}

// Latin_sort関数
function compareLaAsc(a, b){
    var a_txt = $(a).find('td').eq(2).text().toString().toLowerCase();
    var b_txt = $(b).find('td').eq(2).text().toString().toLowerCase();

    if(a_txt < b_txt){
        return -1;
    }else{
        return 1;
    }
}
function compareLaDes(a, b){
    var a_txt = $(a).find('td').eq(2).text().toString().toLowerCase();
    var b_txt = $(b).find('td').eq(2).text().toString().toLowerCase();

    if(b_txt < a_txt){
        return -1;
    }else{
        return 1;
    }
}