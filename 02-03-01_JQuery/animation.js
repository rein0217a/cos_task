var cls_active = "is-active";

$(document).ready(function(){
    var count = 1;
    var interval =  setInterval(function(){
        // バナーtranslate設定
        if(count == 0){
            setTranslate(0);
        }else{
            setTranslate(count);
        }
        if(count == 3){
            clearInterval(interval);
        }
        // ドットボタン設定
        setClassToDots(count);

        count++;
    }, 3000);

    // ドットボタン初期化
    $("li:first", ".banner_dots").addClass(cls_active);
    
    // ドットボタン押下後
    $("a", ".banner_dots").on("click", function(event){
        clearInterval(interval);

        count = parseInt(event.target.title);
        setTranslate(count);
        setClassToDots(count);

        interval =  setInterval(function(){
            // バナーtranslate設定
            if(count == 0){
                setTranslate(0);
            }else{
                setTranslate(count);
            }
            if(count == 3){
                clearInterval(interval);
            }
            // ドットボタン設定
            setClassToDots(count);

            count++;
        }, 3000);
    });
});

// translate設定
function setTranslate(count){
    $(".banner_lst").css("transform", "translateY(calc(-230px * " + count + "))");
}

// ドットクラス設定
function setClassToDots(count){
    var dotsLst = $("li", ".banner_dots");
    dotsLst.removeClass(cls_active);
    for(var i = 0; i < dotsLst.length; i++){
        if(dotsLst[i].firstElementChild.title == count){
            dotsLst[i].classList.add(cls_active);
        }
    }
}