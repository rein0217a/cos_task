$(document).ready(function(){
    // table初期化
    initTable();

    // 行 押下
    $("tr:not(:first)", ".info").on("click", function(){
        var array = [];
        $("td", this).each(function(index){
            array.push($(this).text());
        })
        $("#input_id").val(array[0]);
        $("#input_ja").val(array[1]);
        $("#input_la").val(array[2]);
    });

    // 保存ボタン押下
    $("#btn_save").on("click", function(){
        alert("内容を保存しました。");

        var txt_id = $("#input_id").val();
        var txt_ja = $("#input_ja").val();
        var txt_la = $("#input_la").val();
        
        // 編集対象検索
        $("tr:not(:first)", ".info").each(function(){
            var txt_target = $("td", this)[0].innerHTML;
            // IDに一致する情報を変更
            if(txt_id == txt_target){
                $("td", this)[0].innerHTML = txt_id;
                $("td", this)[1].innerHTML = txt_ja;
                $("td", this)[2].innerHTML = txt_la;

                // inputを空欄にする
                $("#input_id").val("");
                $("#input_ja").val("");
                $("#input_la").val("");
                return true;
            }
        })
    });
});



// table初期化関数
function initTable(){
    let constellationLst = [
        ["牡羊座", "Aries"],
        ["牡牛座", "Taurus"],
        ["双子座", "Gemini"],
        ["蟹座", "Cancer"],
        ["獅子座", "Leo"],
        ["乙女座", "Virgo"],
        ["天秤座", "Libra"],
        ["蠍座", "Scorpio"],
        ["射手座", "Sagittarius"],
        ["山羊座", "Capricorn"],
        ["水瓶座", "Aquarius"],
        ["魚座", "Pisces"]
    ]
    
    for(var i = 0; i < constellationLst.length; i++){
        var str = "<tr>";
        str += "<td>";
        str += i + 1;
        str += "</td>";
        for(var j = 0; j < 2; j++){
            str += "<td>"
            str += constellationLst[i][j];
            str += "</td>";
        }
        str += "</tr>";
        $(".info").append(str);
    }
}