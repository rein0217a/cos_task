// **************************************************
// * ファイル名：./staff-cast/js/base.js                  *
// * 概要     ：staff-castのjs                              *
// **************************************************
// **************************************************
// * 共通                                            *
// **************************************************
// **************************************************
// * イベント設定                                     *
// **************************************************
window.addEventListener("load", staffCast_loadMain, false);


// **************************************************
// * 関数                                            *
// **************************************************
// ロードメイン関数
function staffCast_loadMain(){
    // コメントポップアップ 表示
    const btn_comment_open = document.getElementsByClassName("contents_inner_item_btn");
    for(var i=0; i<btn_comment_open.length; i++){
        btn_comment_open[i].addEventListener("click", function(ele){
            ele.preventDefault();
            var id_targte = this.hash.replace("#","");
            addActById(id_targte);
        },false);
    }

    // コメントポップアップ 非表示
    const btn_commnet_close = document.getElementsByClassName("comment_item_btnClose");
    for(var i=0; i<btn_commnet_close.length; i++){
        btn_commnet_close[i].addEventListener("click", function(ele){
            ele.preventDefault();
            this.parentNode.classList.remove(stts_act);
        },false);
    }
}